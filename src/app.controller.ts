import { Controller, Get, Param, Res } from '@nestjs/common'; 
import { AppService } from './app.service';

@Controller('api')
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('search/:diary') // Endpoint
  getHello(@Param() params, @Res() response) { // Función para tomar los datos de las noticias
    this.appService.getNews(params.diary, response);
  }
}
