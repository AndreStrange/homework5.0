// Vas a construir un agregador de noticias Restful API utilizando las API de The Guardian y
//The New York Time.
// Su API tendrá un único punto final para manejar las búsquedas. OK
// Debería buscar en ambas API. OK
// Debería exponer un filtro que se usa para buscar solo por 1 API de noticias. OK
// La API Restful debe definir una interfaz común para las respuestas de las API de noticias y ambas cargas
// útiles de respuesta a noticias deben ajustarse a ella.
// La API Restful debe estar documentada, ya sea utilizando el archivo README.md, Postman o Swagger.
// Como crédito adicional, solo una de las dos fuentes debe ser accesible para usuarios no autenticados,
// para buscar desde la otra (o ambas al mismo tiempo) el usuario debe estar autenticado (usando JWT).
// Dado que en esta semana no manejaremos una base de datos, el token se puede crear usando https://jwt.io/

import { NestFactory } from '@nestjs/core'; // Importamos módulos de nest
import { AppModule } from './app.module'; // Importamos el archivo app.module

async function bootstrap() { // Función para inicializar el servidor
  const app = await NestFactory.create(AppModule);
  await app.listen(3000);
}
bootstrap();
