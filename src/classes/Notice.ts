import Interfaz from '../interface/interface';
export default class Notice implements Interfaz { // Exportamos la interfaz para la estructura de las noticias
  title: string;
  section: string;
  url: string;
  date: string;

  constructor( // Constructor 
    newTitle: string,
    newSection: string,
    newURL: string,
    newDate: string,
  ) {
    this.title = newTitle;
    this.section = newSection;
    this.url = newURL;
    this.date = newDate;
  }
}
