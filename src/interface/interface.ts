export default interface Interfaz {
  title: string;
  section: string;
  url: string;
  date: string;
}
