import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import jwt from 'jsonwebtoken';
import config from '../config';

@Injectable()
export class AuthMiddleware implements NestMiddleware {
  use(req: Request, res: Response, next: NextFunction) {
    const token = req.headers['x-acces-token'];
    if (req.params.diary === 'nyt' || req.params.diary === 'both') {
      if (token) {
        try {
          next();
        } catch (error) {
          console.log(error);
        }
      } else {
        res.status(403).send({
          message: 'Usted no tiene acceso a este recurso',
        });
      }
    } else {
      next();
    }
  }
}
