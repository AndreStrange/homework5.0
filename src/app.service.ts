import { Injectable } from '@nestjs/common';
import axios from 'axios';
import Notice from './classes/Notice';

@Injectable()
export class AppService { // Exportamos la clase AppService
  getNews(diary: string, response) { // Método para obtener noticia
    if (diary === 'nyt') { // Condicional para cuando el usuario requiera el diario NEW YORK TIMES
      axios 
        .get( // Axios para hacer petición a la API
          'https://api.nytimes.com/svc/topstories/v2/arts.json?api-key=IeVoykq4lTSBV0BftPycT3nq1adHla5b',
        )
        .then((res) => { // Respuesta  
          if (res.data) { // Condicional para cuando encuentre datos en la petición
            response.status(200).json( // Respondemos con un json
              res.data.results.map((notice) => { // Realizamos un map en las noticias
                return new Notice( // Retornamos el objeto con la estructura de la interfaz
                  notice['title'],
                  notice['section'],
                  notice['url'],
                  notice['created_date'],
                );
              }),
            );
          } else { // Condicional por si no encuentra datos en la petición
            response
              .status(500)
              .send({ message: `ERROR: No se encontraron registros` });
          }
        })
        .catch((err) => { // Atrapamos el error
          response.status(500).send({ message: `ERROR: ${err}` });
        });
    } else if (diary === 'tg') { // Condicional para cuando el usuario requiera el diario THE GUARDIANS
      axios 
        .get( // Axios para hacer petición a la API
          'https://content.guardianapis.com/search?api-key=e05eb5ea-bbf3-4422-b88f-f9041498942c',
        )
        .then((res) => { // Respuesta
          if (res.data) { // Condicional para cuando encuentre datos en la petición
            response.status(200).json( // Respondemos con un json
              res.data.response.results.map((notice) => { // Realizamos un map en las noticias
                return new Notice( // Retornamos el objeto con la estructura de la interfaz
                  notice['webTitle'],
                  notice['sectionName'],
                  notice['webUrl'],
                  notice['webPublicationDate'],
                );
              }),
            );
          } else { // Condicional por si no encuentra datos en la petición
            response
              .status(500)
              .send({ message: `ERROR: No se encontraron registros` });
          }
        })
        .catch((err) => { // Atrapamos el error
          response.status(500).send({ message: `ERROR: ${err}` });
        });
    } else if (diary === 'both') { // Condicional para cuando el usuario requiera ambos diarios
      axios 
        .get( // Axios para hacer petición a la API
          'https://api.nytimes.com/svc/topstories/v2/arts.json?api-key=IeVoykq4lTSBV0BftPycT3nq1adHla5b',
        )
        .then((res) => { // Respuesta
          if (res.data) { // Condicional para cuando encuentre datos en la petición
            axios
              .get( // Axios para hacer la petición a la API
                'https://content.guardianapis.com/search?api-key=e05eb5ea-bbf3-4422-b88f-f9041498942c',
              )
              .then((res2) => { // Respuesta 2
                if (res2.data) { // Condicional para cuando encuentre datos en la peticións
                  response.status(200).json( // Respondemos con un json
                    res.data.results
                      .concat(res2.data.response.results) // Concatenamos ambas respuestas
                      .map((notice) => { // Realizamos un map en las noticias
                        return new Notice( // Retornamos el objeto con la estructura de la interfaz
                          notice[notice['webTitle'] ? 'webTitle' : 'title'],
                          notice[
                            notice['sectionName'] ? 'sectionName' : 'section'
                          ],
                          notice[notice['webUrl'] ? 'webUrl' : 'url'],
                          notice[
                            notice['webPublicationDate']
                              ? 'webPublicationDate'
                              : 'created_date'
                          ],
                        );
                      }),
                  );
                } else { // Condicional por si no encuentra datos en la petición 2
                  response
                    .status(500)
                    .send({ message: `ERROR: No se encontraron registros` });
                }
              })
              .catch((err) => { // Atrapamos el error
                response.status(500).send({ message: `ERROR: ${err}` });
              });
          } else { // Condicional por si no encuentra datos en la petición 1
            response
              .status(500)
              .send({ message: `ERROR: No se encontraron registros` });
          }
        })
        .catch((err) => { // Atrapamos el error
          response.status(500).send({ message: `ERROR: ${err}` });
        });
    } else { // Condicional por si no encuentra el diario indicado
      response.status(404).send({ message: 'Diario no encontrado' });
    }
  }
}
